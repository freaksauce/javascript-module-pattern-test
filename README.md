# README #

A simple Javascript Module Pattern test, in fact it is a Revealing Module Pattern to be precise, which sets myName as a private variable, exposes a public setName function which allows you to update myName and finally a public getName function which outputs myName to the console.