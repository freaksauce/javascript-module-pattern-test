var myModule = (function() {
  var myName = "Jon";
  
  function publicGetName() {
    return myName;
  }
  
  function publicSetName(newName) {
    myName = newName;
  }
  
  return {
    getName: publicGetName,
    setName: publicSetName
  };
  
}());

console.log(myModule.getName());
myModule.setName("Paul");
console.log(myModule.getName());